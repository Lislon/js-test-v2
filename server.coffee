express = require 'express'
path = require 'path'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
fs = require 'fs'
errorHandler = require 'errorhandler'
handlebars = require 'express-handlebars'
validator = require 'express-validator'
session = require 'express-session'
require 'coffee-script/register'
mongoose = require 'mongoose'
morgan = require 'morgan'
_ = require 'underscore'
User = require './server/models/user'

routes = require './server/routes'
apiControllers = require './server/controllers/api-controllers'
mainControllers = require './server/controllers/main-controllers'
authControllers = require './server/controllers/auth-controllers'
authenticaton = require './server/authenticaton'


app = express()
app.set 'views', './server/views'
app.engine 'handlebars', handlebars
    extname: '.hbs'
app.set 'view engine', 'handlebars'
app.get '/favicon.ico', (req, res) -> res.send(404)
app.use express.static('./public')
app.use bodyParser.json()
app.use bodyParser.urlencoded
    extended: true
app.use validator()
app.use cookieParser()
app.use session
    secret: process.env.SESSION_SECRET || 'secret',
    resave: false,
    saveUninitialized: false

app.use morgan 'tiny'
authenticaton.setup(app)
routes.setup(app, _.extend(mainControllers, apiControllers, authControllers))

if app.get 'env' == 'development'
    app.use errorHandler
        dumpExceptions: true
        showStack: true
else
    app.use errorHandler()

mongoose.connect 'mongodb://127.0.0.1/js-test', (err) ->
    if err
        console.log err
    else
        app.listen 3000, () ->
            console.log "Server started"
