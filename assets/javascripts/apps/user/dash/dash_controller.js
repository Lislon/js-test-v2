define(["app", "backbone", "backbone.marionette", "jquery", "underscore"], function(App, Backbone, Marionette, $, _) {
    return App.module("AppUser.Dash", function(Dash, App, Backbone, Marionette, $, _) {
        Dash.Controller = Marionette.Controller.extend({
            initialize: function() {
                var that = this;

                var profile = App.request("model:my:profile");
                profile._fetch.success(function() {
                    App.mainRegion.show(that.layout);
                });

                this.layout = new Dash.Layout({});
                this.layout.on("show", function() {
                    var inboxView = new Dash.Inbox({
                        collection: profile.inbox
                    });
                    var outboxView = new Dash.Outbox({
                        collection: profile.outbox
                    });
                    var friendsView = new Dash.Friends({
                        collection: profile.friends
                    });
                    that.layout.inboxRegion.show(inboxView);
                    that.layout.outboxRegion.show(outboxView);
                    that.layout.friendsRegion.show(friendsView);
                });

            }
        });
    });
});
