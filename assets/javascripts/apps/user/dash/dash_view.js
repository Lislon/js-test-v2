define(["app", "backbone", "backbone.marionette", "jquery", "underscore", 
       "hbs!apps/user/dash/templates/layout",
       "hbs!apps/user/dash/templates/inbox",
       "hbs!apps/user/dash/templates/outbox",
       "hbs!apps/user/dash/templates/friends",
       "hbs!apps/user/dash/templates/friend_line"
], function(App, Backbone, Marionette, $, _, 
            TemplateLayout, 
            TemplateInbox, 
            TemplateOutbox, 
            TemplateFriends, 
            TemplateFriendLine) {
    return App.module("AppUser.Dash", function(Dash, App, Backbone, Marionette, $, _) {
        Dash.Layout = Marionette.LayoutView.extend({
            template: TemplateLayout,
            regions: {
                inboxRegion: '#inbox-region',
                outboxRegion: '#outbox-region',
                friendsRegion: '#friends-region'
            }
        });

        Dash.FriendLine = Marionette.ItemView.extend({
            template: TemplateFriendLine,
            tagName: 'li'
        });

        Dash.Friends = Marionette.CompositeView.extend({
            template: TemplateFriends,
            childView: Dash.FriendLine,
            childViewContainer: 'ul'
        });
        Dash.Inbox = Marionette.ItemView.extend({
            template: TemplateInbox
        });
        Dash.Outbox = Marionette.ItemView.extend({
            template: TemplateOutbox
        });
    });
});

