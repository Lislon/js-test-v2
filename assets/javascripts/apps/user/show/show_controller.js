define(["app", "backbone", "backbone.marionette", "jquery", "underscore"], function(App, Backbone, Marionette, $, _) {
    return App.module("AppUser.Show", function(Show, App, Backbone, Marionette, $, _) {
        Show.Controller = Marionette.Controller.extend({
            initialize: function (username) {
                user = App.request('model:user', username);
                App.execute("when:fetched", user, function() {
                    var view = new Show.UserView({
                        model: user
                    });
                    App.mainRegion.show(view);
                });
            },
            show: function(username) {
                
            }
        });
    });
});
