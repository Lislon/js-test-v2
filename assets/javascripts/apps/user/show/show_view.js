define(["app", "backbone", "backbone.marionette", "jquery", "underscore", "hbs!apps/user/show/templates/show"], function(App, Backbone, Marionette, $, _, TemplateShow) {
    return App.module("AppUser.Show", function(Show, App, Backbone, Marionette, $, _) {
        Show.UserView = Marionette.ItemView.extend({
            template: TemplateShow
        });
    });
});
