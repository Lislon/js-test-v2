define(["app", "backbone", "backbone.marionette", "jquery", "underscore"],
    function (App, Backbone, Marionette, $, _) {
    return App.module("AppUser", function(AppUser, App, Backbone, Marionette, $, _) {
        this.startWithParent = false;

        AppUser.Router = Marionette.AppRouter.extend({
            appRoutes: {
                'user/:username' : 'show',
                'dashboard' : 'dashboard'
            }
        });

        API = {
            'show': function(username) {
                require(["apps/user/show/show_view", "apps/user/show/show_controller"], function() {
                    new AppUser.Show.Controller(username);
                });
            },
            'dashboard': function() {
                require(["apps/user/dash/dash_view", "apps/user/dash/dash_controller"], function() {
                    new AppUser.Dash.Controller();
                });
            },
            'addFriend': function() {
                require(["apps/user/dash/dash_view", "apps/user/dash/dash_controller"], function() {
                    new AppUser.Dash.Controller();
                });
            }
        };

        App.addInitializer(function() {
            new AppUser.Router({
                controller: API
            });
        });

        App.vent.on('user:show', function(username) {
            API.show();
        });
    });
});

