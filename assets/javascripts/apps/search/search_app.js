define(["app", "backbone", "backbone.marionette", "jquery", "underscore", 
       "hbs!apps/search/templates/search", "hbs!apps/search/templates/search_item"], 
       function (App, Backbone, Marionette, $, _, 
                 Template, ItemTemplate) {

    return App.module("App.Search", function(AppSearch, App, Backbone, Marionette, $, _) {
        this.startWithParent = false;
        AppSearch.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "search" : 'search'
            }
        });
        
        AppSearch.ItemView = Marionette.ItemView.extend({
            template: ItemTemplate,
            tagName: 'li',
            events: {
                'click .add-friend': function() {
                    App.reqres.request("model:friend:add", this.model.attributes.username);
                }
            }
        });

        var searchResults = new Backbone.Collection();

        AppSearch.View = Marionette.CompositeView.extend({
            template: Template,
            childView: AppSearch.ItemView,
            childViewContainer: '.list-of-users > ul',

            events: {
                'change input.search': 'searchClick'
            },
            searchClick: function() {
                API.doSearch($('input.search').val());
            },
            onDestroy: function() {
                searchResults.reset();
            }
        });

        var API = {
            'search': function(req) {
                var view = new AppSearch.View({
                    collection: searchResults
                });
                App.mainRegion.show(view);

                if (req) {
                    var term = req.substring(2);
                    $("input.search").val(term);
                    this.doSearch(term);
                }
                $('.page').before('<div class="notification">Hello</div>');
            },
            'doSearch': function(searchTerm) {
                Backbone.history.navigate('search?q=' + encodeURIComponent(searchTerm));
                $.ajax({
                    type: 'GET',
                    url: '/api/search?q=' + encodeURIComponent(searchTerm)
                }).done(function(data) {
                    var list = _.map(data.list, function(item) {
                        item.url = '/user/' + item.username;
                        return item;
                    });
                    searchResults.reset(list);
                });
            }
        };

        App.addInitializer(function() {
            new AppSearch.Router({
                controller: API
            });
        });
    });
});
