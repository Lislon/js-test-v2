define(["app", "backbone", "backbone.marionette", "jquery", "underscore"], function (App, Backbone, Marionette, $, _) {
    return App.module("App.Navigation", function(AppNavigation, App, Backbone, Marionette, $, _) {

        AppNavigation.View = Marionette.ItemView.extend({
            template: false,
            el: ".navigation",

            events: {
                "click #nav-logout": function() {
                    $.ajax({
                        url: '/api/logout',
                        method: 'POST',
                        success: function() {
                            console.log("logout ok");
                            window.location = "/login";
                        }
                    });
                    return false;
                }
            }
        });

        App.addInitializer(function() {
            view = new AppNavigation.View();
        });
    });
});
