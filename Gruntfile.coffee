module.exports = (grunt) ->

    grunt.loadNpmTasks 'grunt-contrib-watch'
    grunt.loadNpmTasks 'grunt-contrib-connect'
    grunt.loadNpmTasks 'grunt-nodemon'
    grunt.loadNpmTasks 'grunt-express-server'
    grunt.loadNpmTasks 'grunt-mocha-test'
    grunt.loadNpmTasks 'grunt-contrib-copy'
    #grunt.loadNpmTasks 'grunt-contrib-coffee'

    grunt.initConfig
        pkg: grunt.file.readJSON 'package.json'
        watch:
            client:
                files: [ 'assets/javascripts/**/*' ]
                tasks: [ 'copy:js' ]
            css:
                files: [ 'assets/stylesheets/**/*' ]
                tasks: [ 'copy:public' ]
            server:
                files: [ 'Gruntfile.coffee', 'server.coffee', 'server/**']
                tasks: [ 'express' ]
                options:
                    livereload: true

        express:
            dev:
                options:
                    script: './server.coffee'
                    opts: ['node_modules/coffee-script/bin/coffee'] 
        nodemon:
            dev:
                script: 'server.coffee'
                args: ['dev']
                nodeArgs: ['--debug']
                ext: 'js,coffee'
                watch: ['server', 'server.coffee']
        connect:
            server:
                options:
                    port: 3000
                    base: 'public'
        mochaTest:
            test:
                options:
                    require: 'coffee-script/register'
                src: ['server/test/**/*.coffee']

        copy:
            public:
                expand: true
                flatten: false
                cwd: 'assets/'
                src: ['stylesheets/**', 'fonts/**']
                dest: 'public/'
            js:
                expand: true
                flatten: false
                cwd: 'assets/'
                src: ['javascripts/**']
                dest: 'public/'


    grunt.registerTask 'default', ['copy', 'express', 'watch']
    grunt.registerTask 'test', ['mochaTest']



