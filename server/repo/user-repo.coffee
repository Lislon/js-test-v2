User = require '../models/user'
mongoose = require 'mongoose'
crypto = require 'crypto' 
UserFriends = require '../models/user-friends'
_ = require 'underscore'
async = require 'async'

# Generate unique pair of user freidship. Usernames sorted by alphabet
# Result string is like: md5('lisa:slon') truncated to 24 chars long to fit
# Object Id
getFriendsObjectId = (username1, username2) ->
    if username1 < username2 
       uniqString = "#{username1}:#{username2}" 
    else 
       uniqString = "#{username2}:#{username1}" 

    hash = new crypto.createHash('md5').update(uniqString).digest('hex')[0..23]
    new mongoose.Types.ObjectId(hash)

getUserObjectId = (username) ->
    hash = new crypto.createHash('md5').update(username).digest('hex')[0..23]
    new mongoose.Types.ObjectId(hash)

module.exports = 
    registerUser: (data, done) ->
        newUser = new User
            _id: getUserObjectId data.username.trim()
            username: data.username.trim()
            firstName: data.firstName.trim()
            lastName: data.lastName.trim()
            password: data.password
        newUser.save done
    
    updateUser: (id, data, done) ->
        User.update { _id: new mongoose.Types.ObjectId id }, data, done

    getUserById: (id, done) ->
        User.findOne { username: 'stub1' }, (err, user) ->
            console.log err, user

        #console.log "search fixed..."
        #User.findOne { _id: new mongoose.Types.ObjectId "54912524e1e549d40d50830f" }, (err, user) -> 
            #console.log "Fixed", err, user

        User.findOne { _id: new mongoose.Types.ObjectId id }, done


    getUserByUsername: (username, done) ->
        User.findOne { username: username }, done

    getUserProfile: (username, done) ->

        userId = getUserObjectId username

        # 1. Search all friends of given username
        UserFriends.find()
            .where('friends').equals(userId)
            .where('status').gt(0)
            .exec (err, friendsList) ->
                resultProcessor(err) if err

                friendsList = _.map friendsList, (record) ->
                    return {
                        friend: (f for f in record.friends when f isnt userId.toString())[0]
                        status: record.status
                        initiator: record.initiator
                    }

                # Add current user to list
                relatedUsersIds = _.pluck friendsList, 'friend'
                relatedUsersIds.push userId

                User.find()
                    .where('_id').in(relatedUsersIds)
                    .exec (err, usersProfiles) ->
                        resultProcessor(err, friendsList, usersProfiles)

        # Builds a final profile
        resultProcessor = (err, friendsList, usersProfiles) ->

            return done(err) if err

            # Make hashmap {id1: user1, id2: user2}
            userXProfile = []
            for up in usersProfiles
                userXProfile[up._id.toString()] = up

            # Return n array of user profiles which are in friends list matching predicate
            findAndGetProfile = (predicate) ->
                result = (userXProfile[f.friend] for f in friendsList when predicate(f))
                result = _.map result, (row) -> _.pick row, 'username', 'firstName', 'lastName'
                result


            # Find current user
            me = userXProfile[userId.toString()]

            outcome = findAndGetProfile (f) -> f.status == UserFriends.STATUS_REQUEST and f.initiator == userId.toString()
            income  = findAndGetProfile (f) -> f.status == UserFriends.STATUS_REQUEST and f.initiator != userId.toString()
            friends = findAndGetProfile (f) -> f.status == UserFriends.STATUS_FRIENDS

            result = {
                username: me.username
                firstName: me.firstName
                lastName: me.lastName
                income: income
                outcome: outcome
                friends: friends
            }
            done(null, result)

    getUserFriends: (user, done) ->
        []

    addFriend: (initiatorUsername, friendUsername, done) ->
        relationId = getFriendsObjectId(initiatorUsername, friendUsername)
        UserFriends.findOne({ _id: relationId }).exec (err, record) ->

            if err
                return done(err)
            if !record
                record = new UserFriends
                    _id: relationId
                    initiator: getUserObjectId initiatorUsername
                    status: UserFriends.STATUS_REQUEST
                    friends: [
                        getUserObjectId initiatorUsername
                        getUserObjectId friendUsername
                    ]
               #console.log "New Friend obj initiator: #{initiatorUsername}"
                #console.log record


            else if record.status == UserFriends.STATUS_NONE
                record.status = STATUS_REQUEST
            else if record.status == UserFriends.STATUS_REQUEST and record.initiator == getUserObjectId(friendUsername).toString()
                record.status = UserFriends.STATUS_FRIENDS
            #console.log "record.initiator: #{record.initiator}"
            #console.log "friendUsername (#{friendUsername}): " + getFriendsObjectId(friendUsername).toString()



            record.save (err, savedItem) ->
                done err, savedItem.status

    deleteFriend: (initiatorUsername, friendUsername, done) ->
        UserFriends.findOne({ _id: getFriendsObjectId(initiatorUsername, friendUsername) }).exec (err, record) ->
            if err
                return done(err)
            if !record
                return done null, UserFriends.STATUS_NONE
            
            record.status = UserFriends.STATUS_NONE
            record.save (err) ->
                done err, record.status

    # Search by "Boris Avdeev" or just "Boris"
    search: (query, done) ->
        directSearch = null
        reverseSearch = null
        nameParts = query.split /\s+/ 

        if nameParts.length == 1
            # Searches like 'James' or 'Bond'
            directSearch = 
                firstName : nameParts[0]
            reverseSearch = 
                lastName: nameParts[0] 
        else
            # Searches like 'James Bond' or 'Bond James'
            directSearch =
                firstName : nameParts[0], 
                lastName : nameParts[1]
            reverseSearch =
                firstName : nameParts[1], 
                lastName : nameParts[0] 

        query = { $or: [ directSearch, reverseSearch ]}
        console.log("Search for ", query)
        User.
            find(query).
            limit(1000).
            select('_id username firstName lastName').
            exec(done)

