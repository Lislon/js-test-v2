exports.setup = (app, handlers) ->

    app.get '/', handlers.index
    app.get '/login', handlers.auth.loginPage
    app.post '/login', handlers.auth.login
    app.post '/logout', handlers.auth.logout

    app.all '/*', handlers.auth.verifyAuth

    app.get '/api/me', handlers.api.myProfile
    app.get '/api/users/:username', handlers.api.profile
    app.get '/api/me/friends', handlers.api.friendList
    app.put '/api/me/friends', handlers.api.friendAdd
    app.delete '/api/me/friends', handlers.api.friendDelete

    app.get '/api/search', handlers.api.search
    app.get '/*', handlers.index
