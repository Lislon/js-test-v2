passport = require 'passport'
userRepo = require './repo/user-repo'
passportLocal = require 'passport-local'

module.exports.setup = (app) ->
    app.use passport.initialize()
    app.use passport.session()

    passport.use new passportLocal.Strategy (username, password, next) ->
        userRepo.getUserByUsername username, (err, user) -> 
            if err 
                return next err
            if !user
                return next null, null, 
                    message: "Unknown user #{username}"
            user.comparePassword password, (err, isMatch) ->
                if err 
                    return next err
                if isMatch
                    return next null, user
                else
                    return next null, null,
                        message: "Invalid password"

    passport.serializeUser (user, done) ->
        #console.log "serialize #{user._id}"
        done null, user._id

    passport.deserializeUser (id, done) ->
        #debugger
        #console.log "deserialize #{id}"

        userRepo.getUserById id, (err, user) -> 
            #console.log "deserialize done: #{user}"

            done err, user

