async = require 'async'
assert = require 'assert'
userRepo = require '../repo/user-repo'
User = require '../models/user'
mongoose = require 'mongoose'
should = require 'should'


describe 'routes', ->

    lisaData = 
        username: 'lisa'
        firstName: 'Lisa'
        lastName: 'Slonova'
        password: 'foo'
    slonData = 
        username: 'slon'
        firstName: 'Slon'
        lastName: 'Lisovich'
        password: 'foo'

    before (done) ->
        mongoose.connect 'mongodb://localhost/js-test-test'
        mongoose.connection.on 'connected', done

    beforeEach (done) ->
        mongoose.connection.db.dropDatabase()

    describe 'authentication', ->
        it 'should register user', (done) ->
            userRepo.registerUser lisaData, ->
                User.findOne { username: 'lisa' }, (err, user) ->
                    lisaData.firstName.should.equal(user.firstName)
                    done(err)

        it 'should add a friend to request list', (done) ->
            async.parallel [
                    (callback) -> userRepo.registerUser(lisaData, callback)
                    (callback) -> userRepo.registerUser(slonData, callback)
                ], (err) ->
                    throw err if err
                    userRepo.addFriend 'slon', 'lisa', (err) ->
                        async.parallel [
                            (callback) -> userRepo.getUserProfile 'slon', callback
                            (callback) -> userRepo.getUserProfile 'lisa', callback
                        ], (err, results) ->
                            [slonProfile, lisaProfile] = results

                            slonProfile.income.should.have.length 0
                            slonProfile.friends.should.have.length 0
                            slonProfile.outcome.should.have.length 1
                            slonProfile.outcome[0].username.should.eql 'lisa'
                            lisaProfile.income[0].username.should.eql 'slon'

                            done(err)

           it "should accept a friend request", (done) ->
               async.series [
                       (callback) -> userRepo.registerUser(lisaData, callback)
                       (callback) -> userRepo.registerUser(slonData, callback)
                       (callback) -> userRepo.addFriend 'slon', 'lisa', callback
                       (callback) -> userRepo.addFriend 'lisa', 'slon', callback
                   ], (err) ->
                       async.parallel [
                           (callback) -> userRepo.getUserProfile 'slon', callback
                           (callback) -> userRepo.getUserProfile 'lisa', callback
                       ], (err, results) ->
                           [slonProfile, lisaProfile] = results

                           slonProfile.friends[0].username.should.eql 'lisa'
                           lisaProfile.friends[0].username.should.eql 'slon'
                           done(err)

