mongoose = require 'mongoose'
bcrypt = require 'bcrypt'
SALT_WORK_FACTOR = 10

usersSchema = new mongoose.Schema
    _id: { type: String, max: 24 }
    username: { type: String, required: true, index: { unique: true } }
    password: { type: String, required: true }
    firstName: String
    lastName: String
    registeredAt: { type: Date, 'default': Date.now }

# We have to search fast by any combination of first & last name
usersSchema.index { firstName: 1, lastName: 1}
usersSchema.index { lastName: 1, firstName: 1}

# Virtual field (Not persisted in Mongo)
usersSchema.virtual('fullName').get () ->
    @firstName + ' ' + @lastName


# Intercept saving for hashing password
usersSchema.pre 'save', (next) ->
    user = @

    # Do rehash only if password modified
    if !user.isModified 'password'
        return next()

    bcrypt.genSalt SALT_WORK_FACTOR, (err, salt) ->
        if err
            return next(err)

        # generate hash of password with salt
        bcrypt.hash user.password, salt, (err, hash) ->
            if (err) 
                return next(err)
            
            # override cleartext pass with the hashed one
            user.password = hash
            next()

# Verify password method
usersSchema.methods =

    comparePassword: (candidatePass, done) ->
        bcrypt.compare candidatePass, @password, (err, isMatch) ->
            if err
                return done(err)
            done(null, isMatch)

    addFriend: (friend, done) ->
        # Already friend? Do nothing
        if friend in @friends or friend in @outbox
            return done null
        # User already requested friendship with you, make you both friends
        if friend in @inbox
            # Remove from inbox
            @inbox[t..t] = [] if (t = @inbox.indexOf(friend)) > -1
            @friends.push friend
            return @save(done)
        else
            @outbox


module.exports = mongoose.model('user', usersSchema)
