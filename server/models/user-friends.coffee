mongoose = require 'mongoose'
_ = require 'underscore'

userFriendsSchema = new mongoose.Schema
    friends: [{ type: String, ref: 'User', capped: 2 }]
    initiator: { type: String, ref: 'User' }
    status: Number

userFriendsSchema.index { friends: 1 }

model = mongoose.model('friends', userFriendsSchema)

_.extend model,
    STATUS_NONE: 0
    STATUS_REQUEST: 1
    STATUS_FRIENDS: 2

module.exports = model
