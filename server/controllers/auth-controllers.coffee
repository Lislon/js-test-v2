passport = require 'passport'
userRepo = require '../repo/user-repo'
_ = require 'underscore'
#UserModel= require '../models/user'

authError = (req, res, errorText) ->

    res.render 'login',
        errorText: errorText

authSuccess = (req, res, next, user) ->

    req.logIn user, (err) ->
        return next(err) if err
        res.redirect '/'

api = 
    auth:
        loginPage: (req, res, next) ->
            demo = no
            if demo
                req.body.username = 'stub1'
                req.body.password = 'stub1'
                req.body.name = ''
                api.auth.login(req, res, next)
            else
                res.render('login')
        login: (req, res, next) ->
            passAuth = passport.authenticate 'local', (err, user, info) ->
                return next(err) if err

                # Validate input
                req.checkBody('username', 'Username is required').notEmpty()
                req.checkBody('password', 'Password is required').notEmpty()

                if (errors = req.validationErrors())
                    return authError req, res, (_.pluck errors, 'msg').join(', ')

                [username, password, name] = [req.body.username, req.body.password, req.body.name]

                if name
                    split = name.split /\s+/
                    if split.length != 2
                        return authError req, res, "Full name should contain both first and last name, like 'Boris Avdeev'"
                    [firstName, lastName] = split
                else
                    firstName = lastName = ''

                # 1. Simple login
                if user && !name
                    return authSuccess req, res, next, user
                
                # 2. Register new user
                else if !user && name
                    console.log "Registering user..."

                    data = 
                        username: username
                        password: password
                        firstName: firstName
                        lastName: lastName

                    userRepo.registerUser data, (err, newUser) ->
                        console.log "New user registered #{username}"
                        authSuccess req, res, next, newUser
                # 3. Change user name
                else if user && name
                    userRepo.updateUser user._id, 
                        firstName: firstName
                        lastName: lastName
                        , (err) ->
                            console.log "Name of #{username} changed to #{name}"
                            authSuccess req, res, next, user

                # 4. Wrong username/password
                else
                    authError req, res, info.message

            passAuth(req, res, next)

        logout: (req, res) ->
            req.logout
            res.redirect '/'
        verifyAuth: (req, res, next) ->
            if req.isAuthenticated()
                next()
            else
                if req.xhr
                    res.status(401).end()
                else
                    res.redirect '/login'

module.exports = api
