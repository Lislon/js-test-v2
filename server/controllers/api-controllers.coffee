userRepo = require '../repo/user-repo'

module.exports = 
    api:
        myProfile: (req, res) ->
            res.json
                me: true

        friendList: (req, res) ->
            res.json
                msg: 'Friends list'

        friendAdd: (req, res) ->
            res.json
                msg: 'Friend added'

        friendDelete: (req, res) ->
            res.json
                msg: 'Friend deleted'

        profile: (req, res) ->
            userRepo.getUserByUsername req.params.username, (err, user) ->
                throw err if err
                if user
                    res.send user
                else
                    res.status(404).end()

        search: (req, res) ->
            if req.query.q 
                userRepo.search req.query.q, (err, results) -> 
                    throw err if err
                    console.log(err, results)
                    userFriends = results.map (foundUser) ->
                        return {
                            fullName: foundUser.fullName
                            username: foundUser.username
                        }
                            #isFriend: foundUser._id in req.user.friends

                    res.send {list: userFriends}
